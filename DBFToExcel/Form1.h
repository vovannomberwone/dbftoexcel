#pragma once

#include <stdio.h>
#include <cstdlib>
#include "wchar.h"
#include <locale>
#include <time.h>
#include <locale.h>
#include <vector>
#include "libxl.h"
#include <Windows.h>
#include "Commdlg.h"
#include <cstring>
#define fstfield 13
#define ctdimfield 24

namespace DBFToExcel {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Ñâîäêà äëÿ Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: äîáàâüòå êîä êîíñòðóêòîðà
			//
		}

	protected:
		/// <summary>
		/// Îñâîáîäèòü âñå èñïîëüçóåìûå ðåñóðñû.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

	protected: 
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;



	protected: 

	private:
		/// <summary>
		/// Òðåáóåòñÿ ïåðåìåííàÿ êîíñòðóêòîðà.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Îáÿçàòåëüíûé ìåòîä äëÿ ïîääåðæêè êîíñòðóêòîðà - íå èçìåíÿéòå
		/// ñîäåðæèìîå äàííîãî ìåòîäà ïðè ïîìîùè ðåäàêòîðà êîäà.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(33, 52);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(125, 35);
			this->button1->TabIndex = 1;
			this->button1->Text = L"�������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(39, 24);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(110, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"�������� DBF ����";
			this->label1->Click += gcnew System::EventHandler(this, &Form1::label1_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->Filter = L"DBF files| *.dbf";
			this->openFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog1_FileOk_2);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(192, 136);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button1);
			this->Name = L"Form1";
			this->Text = L"DBFToExcel";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void openFileDialog1_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 }
	private: System::Void openFileDialog1_FileOk_1(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 }

private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
		 }

private:

char* makestr(int i, int side, char *str) {
    str[0] = 'F';
    str[1] = '_';
    str[2] = i / 10 + '0';
    str[3] = i % 10 + '0';
    str[4] = '_';
    str[5] = side + '0';
    str[6] = '\0';
    return(str);
}

unsigned int makenum(unsigned char *buf, unsigned int size) {
	unsigned int num = 0;
	int i;
	if (size == 2) {
		num = buf[1];
		num <<= 8;
		num += buf[0];
	} else {
		for (i = 3; i >= 0; i--) {
			num <<= 8;
			num += buf[i];
		}
	}
	return num;
}

unsigned int makeint(unsigned char *buf) {
	unsigned int num = 0;
	int i;
	for (i = 0; i < 4; i++) {
		if (buf[i] == 0x20) {
			continue;
		} else {
			num = (num * 10) + (buf[i] - '0');
		}
	}
	return num;
}

void makedate(char *str) {
	char temp;
	temp = str[0];
	str[0] = str[6];
	str[6] = temp;
	temp = str[1];
	str[1] = str[7];
	str[7] = temp;
	str[8] = str[2];
	str[9] = str[3];
	str[3] = str[4];
	str[4] = str[5];
	str[2] = '.';
	str[5] = '.';
	str[10] = '\0';
	return;
}

double makeage(char *f_bdate, struct tm *timeinfo) {
	double age;
	age = 1900 + timeinfo->tm_year - (f_bdate[6] - '0') * 1000 - (f_bdate[7] - '0') * 100 -
	(f_bdate[8] - '0') * 10 - (f_bdate[9] - '0') +
	(timeinfo->tm_mon - (f_bdate[3] - '0') * 10 - (f_bdate[4] - '0')) / (double)12 +
	(timeinfo->tm_mday - (f_bdate[0] - '0') * 10 - (f_bdate[1] - '0')) / (double)365;
	return age;
}

void correct(char *str) { // for CP866
	while(*str != '\0') {
		if (*str <= -128 + 48 && *str >= -128) {
			*str = *str + 64;
		} else {
			*str = *str + 16;
		}
		str++;
	}
	return;
}

void cpstr(char *str1, char *str2) {
    while (*str2) {
        *str1++ = *str2++;
    }
    *str1 = '\0';
    return;
}

int mstrlen(char *str) {
	int leng = 0;
	while (*str != '\0') {
		str++;
		leng++;
	}
	return leng;
}

void cpfile(char *source, char *target_tmp) {
	char sys_call[400];
	char target[200];
    sprintf(target, "results\\%s", target_tmp);
	sprintf(sys_call, "copy %s %s\n", source, target);
    system(sys_call);
    return;
}

const wchar_t *GetWC(const char *c, std::vector<wchar_t> &vec) {
    size_t len = strlen(c);
    vec.resize(len+1);
    mbstowcs(&vec[0], c, len);
	vec[len] = 0;
    const wchar_t* wc = &vec[0];

    return wc;
}

private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	
	FILE *fp_dbf_file, *norm_woman, *norm_man, *coef_woman, *coef_man, *fp_coef, *fp_norm;
	unsigned int offset, svoffset, headsz, recsz;
	int c, i, cur_rec, col, ctfield, ctrec;
	double sr, age, coef, norm, difnorm;
	unsigned char buf[5];
	int dim[100];
	enum sem {
		f_r_e_e, P, MC, C, Ig, TR, Gi, RP, F, R, V, VB, E
	};
	double res[100], difsr[100], dif_sr_perc[100], asym[100], res_asym[100];
	unsigned char fieldsize[100];
	char dbf_file[100];
	char f_surname[40], f_name[6], f_sex[2], f_mdate[11], f_mtime[6], f_bdate[11];
	const double manl[] = {
		16.5, 22.78, 27.86, 34.72, 43.01, 52.28, 63.71, 07.00, 11.00, 14.00
	};
	const double manh[] = {
		22.78, 27.86, 34.72, 43.01, 52.28, 63.71, 99.00, 11.00, 14.00, 16.5
	};
	const double womanl[] = {
		18.33, 21.97, 27.22, 34.07, 42.20, 51.43, 60.46, 04.00, 06.00, 14.00
	};
	const double womanh[] = {
		21.97, 27.22, 34.07, 42.20, 51.43, 60.46, 99.00, 06.00, 14.00, 18.33
	};
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	std::vector<wchar_t> vec;

	bool valid = false;
	do {
		openFileDialog1->ShowDialog();
		using namespace Runtime::InteropServices;
		char* t_str = (char*)(Marshal::StringToHGlobalAnsi(openFileDialog1->FileName)).ToPointer();
		strcpy(dbf_file, t_str);
		if ((fp_dbf_file = fopen(dbf_file, "r")) != NULL) {
			valid = true;
		} else {
			char t_str2[200];
			strcpy(t_str2, "Cant open file: ");
			strcat(t_str2, dbf_file);
			strcat(t_str2, " \nWant to continue?");
			System::Windows::Forms::DialogResult result = MessageBox::Show(
				gcnew System::String(t_str2),
				gcnew System::String("Error"),
				MessageBoxButtons::YesNo,
				MessageBoxIcon::Error
			);
			if (result == System::Windows::Forms::DialogResult::No) {
				this->Close();
				return;
			}
		}
	} while (!valid);

	if (NULL == (norm_woman = fopen("norm_woman.txt", "rb"))) {
		char t_str2[200];
		strcpy(t_str2, "Cant open file: norm_woman");
		System::Windows::Forms::DialogResult result = MessageBox::Show(
			gcnew System::String(t_str2),
			gcnew System::String("Error"),
			MessageBoxButtons::OK,
			MessageBoxIcon::Error
		);
		this->Close();
		return;
	}
	if (NULL == (norm_man = fopen("norm_man.txt", "rb"))) {
		char t_str2[200];
		strcpy(t_str2, "Cant open file: norm_man");
		System::Windows::Forms::DialogResult result = MessageBox::Show(
			gcnew System::String(t_str2),
			gcnew System::String("Error"),
			MessageBoxButtons::OK,
			MessageBoxIcon::Error
		);
		this->Close();
		return;
	}
	if (NULL == (coef_man = fopen("coef_man.txt", "rb"))) {
		char t_str2[200];
		strcpy(t_str2, "Cant open file: coef_man");
		System::Windows::Forms::DialogResult result = MessageBox::Show(
			gcnew System::String(t_str2),
			gcnew System::String("Error"),
			MessageBoxButtons::OK,
			MessageBoxIcon::Error
		);
		this->Close();
		return;
	}
	if (NULL == (coef_woman = fopen("coef_woman.txt", "rb"))) {
		char t_str2[200];
		strcpy(t_str2, "Cant open file: coef_woman");
		System::Windows::Forms::DialogResult result = MessageBox::Show(
			gcnew System::String(t_str2),
			gcnew System::String("Error"),
			MessageBoxButtons::OK,
			MessageBoxIcon::Error
		);
		this->Close();
		return;
	}
    BookHandle book;

	fseek(fp_dbf_file, 4, SEEK_SET);
	fread(buf, 4, 1, fp_dbf_file);
	ctrec = makenum(buf, 4);
	fread(buf, 2, 1, fp_dbf_file);
	headsz = makenum(buf, 2);
	ctfield = (headsz - (32 + 1)) / 32;
	fread(buf, 2, 1, fp_dbf_file);
	recsz = makenum(buf, 2);
	fseek(fp_dbf_file, 32 + 16, SEEK_SET);
	for (i = 0; i < ctfield; i++) {
		fread(&(fieldsize[i + 1]), 1, 1, fp_dbf_file);
		fseek(fp_dbf_file, (32 + 16) + 32 * (i + 1), SEEK_SET);
	}
	for (cur_rec = 1; cur_rec <= ctrec; cur_rec++) {
        book = xlCreateXMLBook();
        if (0 == xlBookLoad(book, L"standartbook.xlsx")) {
            printf("%s\n", xlBookErrorMessage(book));
			fprintf(stderr, "Cant open file standartbook.xlsx please ");
			fprintf(stderr, "make sure that it exists\n push ENTER\n");
			while ((c = getchar()) != '\n');
	        return;
		}
        SheetHandle sheet = xlBookGetSheet(book, 0);

		offset = headsz;
		for (i = 1; i < cur_rec; i++) {
			offset += recsz;
		}
		//miss del(20h) byte
		offset++;
		svoffset = offset;
		fseek(fp_dbf_file, offset, SEEK_SET);
		fread(f_surname, fieldsize[1], 1, fp_dbf_file);
		for (i = 0; i < fieldsize[1]; i++) {
			if (f_surname[i] == 0x20) {
				f_surname[i] = '\0';
				break;
			}
        }
		f_surname[fieldsize[1]] = '\0';
		correct(f_surname);
		fread(f_name, 4, 1, fp_dbf_file);
		correct(f_name);
		f_name[1] = '.';
		f_name[3] = '.';
		f_name[4] = '\0';

		offset = svoffset;
		//f_sex == 5
		for (i = 1; i < 5; i++) {
			offset += fieldsize[i];
		}
		fseek(fp_dbf_file, offset, SEEK_SET);
		fread(f_sex, 1, 1, fp_dbf_file);
		f_sex[1] = '\0';
		correct(f_sex);

		fread(f_mdate, 8, 1, fp_dbf_file);
		makedate(f_mdate);
		fread(f_mtime, 5, 1, fp_dbf_file);
		f_mtime[5] = '\0';
		fread(f_bdate, 8, 1, fp_dbf_file);
		makedate(f_bdate);

		offset = svoffset;
		for (i = 1; i < fstfield; i++) {
			offset += fieldsize[i];
		}
		fseek(fp_dbf_file, offset, SEEK_SET);
		sr = 0;
		for (i = 1; i <= ctdimfield; i++) {
			fread(buf, 4, 1, fp_dbf_file);
			dim[i] = makeint(buf);
			xlSheetWriteNum(sheet, 5 + 49, i, dim[i], 0);
			sr += dim[i];
		}
		sr /= ctdimfield;
		xlSheetWriteNum(sheet, 2, 8, sr, 0);
		xlSheetWriteNum(sheet, 2 + 49, 8, sr, 0);
		for (i = 1; i <= ctdimfield; i++) {
			difsr[i] = dim[i] - sr;
			xlSheetWriteNum(sheet, 7 + 49, i, difsr[i], 0);
		}
		for (i = 1; i <= ctdimfield / 2; i++) {
			asym[i] = (dim[i] - dim[i + ctdimfield / 2]) * 100.0 / (dim[i] + dim[i + ctdimfield / 2]) / 2;
			dif_sr_perc[i] = (difsr[i] + difsr[i + ctdimfield / 2]) / 2 * 100 / sr;
			xlSheetWriteNum(sheet, 10 + 49, i, dif_sr_perc[i], 0);
		}
		setlocale( LC_ALL, "" );
		xlSheetWriteStr(sheet, 2, 1, GetWC(f_surname, vec), 0);
		xlSheetWriteStr(sheet, 2 + 49, 1, GetWC(f_surname, vec), 0);
		xlSheetWriteStr(sheet, 2, 2, GetWC(f_name, vec), 0);
		xlSheetWriteStr(sheet, 2 + 49, 2, GetWC(f_name, vec), 0);
		xlSheetWriteStr(sheet, 2, 3, GetWC(f_sex, vec), 0);
		xlSheetWriteStr(sheet, 2 + 49, 3, GetWC(f_sex, vec), 0);
		xlSheetWriteStr(sheet, 2, 4, GetWC(f_mdate, vec), 0);
		xlSheetWriteStr(sheet, 2 + 49, 4, GetWC(f_mdate, vec), 0);
		xlSheetWriteStr(sheet, 2, 5, GetWC(f_mtime, vec), 0);
		xlSheetWriteStr(sheet, 2 + 49, 5, GetWC(f_mtime, vec), 0);
		xlSheetWriteStr(sheet, 2, 6, GetWC(f_bdate, vec), 0);
		xlSheetWriteStr(sheet, 2 + 49, 6, GetWC(f_bdate, vec), 0);
		age = makeage(f_bdate, timeinfo);
		xlSheetWriteNum(sheet, 2, 7, age, 0);
		xlSheetWriteNum(sheet, 2 + 49, 7, age, 0);
		res[1] = (dif_sr_perc[P] + dif_sr_perc[MC]) / 2;
        res[2] = (dif_sr_perc[P] + dif_sr_perc[MC] + dif_sr_perc[F]) / 3;
        res[3] = (dif_sr_perc[C] + dif_sr_perc[F]) / 2;
        res[4] = (dif_sr_perc[C]);
        res[5] = (dif_sr_perc[C] + dif_sr_perc[TR]) / 2;
        res[6] = (dif_sr_perc[TR]);
        res[7] = (dif_sr_perc[Ig] + dif_sr_perc[TR]) / 2;
        res[8] = (dif_sr_perc[P] + dif_sr_perc[Ig]) / 2;
        res[9] = (dif_sr_perc[P]);
        res[10] = (dif_sr_perc[VB] + dif_sr_perc[P] + dif_sr_perc[C]) / 3;
        res[11] = (dif_sr_perc[VB] + dif_sr_perc[P] + dif_sr_perc[C] + dif_sr_perc[MC]) / 4;
        res[12] = (dif_sr_perc[E] + dif_sr_perc[C] + dif_sr_perc[MC]) / 3;
        res[13] = (dif_sr_perc[E] + dif_sr_perc[RP] + dif_sr_perc[C]) / 3;
        res[14] = (dif_sr_perc[E] + dif_sr_perc[RP] + dif_sr_perc[F]) / 3;
        res[15] = (dif_sr_perc[E] + dif_sr_perc[RP] + dif_sr_perc[F]) / 3;
        res[16] = (dif_sr_perc[F] + dif_sr_perc[R]) / 2;
        res[17] = (dif_sr_perc[VB] + dif_sr_perc[F] + dif_sr_perc[R]) / 3;
        res[18] = (dif_sr_perc[VB] + dif_sr_perc[RP] + dif_sr_perc[V] + dif_sr_perc[R]) / 4;
        res[19] = (dif_sr_perc[Ig] + dif_sr_perc[E] + dif_sr_perc[RP] + dif_sr_perc[V]) / 4;
        res[20] = (dif_sr_perc[Ig] + dif_sr_perc[E] + dif_sr_perc[V] + dif_sr_perc[TR]) / 4;
        res[21] = (dif_sr_perc[Ig] + dif_sr_perc[R] + dif_sr_perc[TR]) / 3;
        res[22] = (dif_sr_perc[R]);
        res[23] = (dif_sr_perc[Gi] + dif_sr_perc[R]) / 2;
        res[24] = (dif_sr_perc[Gi]);
        res[25] = (dif_sr_perc[Gi]);
        res[26] = (dif_sr_perc[Gi] + dif_sr_perc[V]) / 2;
        res[27] = (dif_sr_perc[Gi] + dif_sr_perc[V]) / 2;
        res[28] = (dif_sr_perc[Gi]);
        res[29] = (dif_sr_perc[Gi]);
        res_asym[1] = (asym[P] + asym[MC]) ;
        res_asym[2] = (asym[P] + asym[MC] + asym[F]) ;
        res_asym[3] = (asym[C] + asym[F]) ;
        res_asym[4] = (asym[C]);
        res_asym[5] = (asym[C] + asym[TR]) ;
        res_asym[6] = (asym[TR]);
        res_asym[7] = (asym[Ig] + asym[TR]) ;
        res_asym[8] = (asym[P] + asym[Ig]) ;
        res_asym[9] = (asym[P]);
        res_asym[10] = (asym[VB] + asym[P] + asym[C]) ;
        res_asym[11] = (asym[VB] + asym[P] + asym[C] + asym[MC]) ;
        res_asym[12] = (asym[E] + asym[C] + asym[MC]) ;
        res_asym[13] = (asym[E] + asym[RP] + asym[C]) ;
        res_asym[14] = (asym[E] + asym[RP] + asym[F]) ;
        res_asym[15] = (asym[E] + asym[RP] + asym[F]) ;
        res_asym[16] = (asym[F] + asym[R]) ;
        res_asym[17] = (asym[VB] + asym[F] + asym[R]) ;
        res_asym[18] = (asym[VB] + asym[RP] + asym[V] + asym[R]) ;
        res_asym[19] = (asym[Ig] + asym[E] + asym[RP] + asym[V]) ;
        res_asym[20] = (asym[Ig] + asym[E] + asym[V] + asym[TR]) ;
        res_asym[21] = (asym[Ig] + asym[R] + asym[TR]) ;
        res_asym[22] = (asym[R]);
        res_asym[23] = (asym[Gi] + asym[R]) ;
        res_asym[24] = (asym[Gi]);
        res_asym[25] = (asym[Gi]);
        res_asym[26] = (asym[Gi] + asym[V]) ;
        res_asym[27] = (asym[Gi] + asym[V]) ;
        res_asym[28] = (asym[Gi]);
        res_asym[29] = (asym[Gi]);
        for (i = 1; i < 30; i++) {
			xlSheetWriteNum(sheet, 13 + 49, i, res[i], 0);
			if (res_asym[i] > 20) {
				xlSheetWriteNum(sheet, 19 + 49, i, res_asym[i] * 3 / 4.47, 0);
				// approxim result
			} else if (res_asym[i] < -20) {
				xlSheetWriteNum(sheet, 19 + 49, i, res_asym[i] * 3 / 4.47, 0);
			} else {
				xlSheetWriteNum(sheet, 19 + 49, i, res_asym[i], 0);
			}
		}
        col = 0;
        if (f_sex[0] == '�') {
        	fp_coef = coef_woman;
        	fp_norm = norm_woman;
        	for (i = 0; i < 10; i++) {
        		if ((age > womanl[i]) && (age <= womanh[i])) {
        			col = i;
        			break;
        		}
        	}
        } else {
        	fp_coef = coef_man;
        	fp_norm = norm_man;
        	for (i = 0; i < 10; i++) {
        		if ((age > manl[i]) && (age <= manh[i])) {
        			col = i;
        			break;
        		}
        	}
        }
        fseek(fp_norm, (29 * col) * sizeof(norm), SEEK_SET);
        fseek(fp_coef, (29 * col) * sizeof(norm), SEEK_SET);
        for (i = 1; i < 30; i++) {
        	fread(&norm, sizeof(norm), 1, fp_norm);
        	fread(&coef, sizeof(coef), 1, fp_coef);
        	difnorm = (res[i] - norm);
        	xlSheetWriteNum(sheet, 15 + 49, i, difnorm, 0);
			if (difnorm / coef > 80) {
				//approxim result
				xlSheetWriteNum(sheet, 17 + 49, i, difnorm / coef * 3 / 8.94, 0); // 8.94 = sqrt(80)
			} else if (difnorm / coef < -80) {
				xlSheetWriteNum(sheet, 17 + 49, i, difnorm / coef * 3 / 8.94, 0);
			} else {
        		xlSheetWriteNum(sheet, 17 + 49, i, difnorm / coef, 0);
        	}
        }
        xlBookSave(book, L"standartbook.xlsx");
        xlBookRelease(book);
        cpstr(f_surname + mstrlen(f_surname), ".xlsx");
        cpfile("standartbook.xlsx", f_surname);
	}
	System::Windows::Forms::DialogResult result = MessageBox::Show(
		gcnew System::String("��������� ��������� ���� ������, �������� ����������\n"),
		gcnew System::String("DBFToExcel"),
		MessageBoxButtons::OK,
		MessageBoxIcon::Information
	);
	fclose(norm_man);
	fclose(norm_woman);
	fclose(coef_man);
	fclose(coef_woman);

	return;

	
		 }
private: System::Void openFileDialog1_FileOk_2(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
		 }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
		 }
};
}

